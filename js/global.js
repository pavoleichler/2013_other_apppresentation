
/**
 * Creates a new AppPrezi instance.
 * 
 * @param {DOMElement|String} [element] The containing element.
 * @returns {AppPrezi}
 */
var AppPrezi = function(element){

    this._init(element);
    
};

/**
 * Inititates the associated DOM elements.
 * 
 * @param {DOMElement|String} [element] The containing element.
 * @returns {undefined}
 */
AppPrezi.prototype._init = function(element){
    
    var self = this;
    
    // find DOM elements
    this.$container = (typeof element !== 'undefined') ? $(element) : $('#appprezi');
    this.$slides = this.$container.children('section');
    this.navigationArrows = {next: null, previous: null};
    
    // initiates the DOM
    this._initDom();
    
    // bind events
    this._bind();
    
    // temporarily disable CSS animations
    this.$slides.css('transition', 'none');
    
    // start from the first slide
    this.reset();
    
    // enable CSS animations again
    setTimeout(function(){
        self.$slides.css('transition', '');
    });
    
};

AppPrezi.prototype._initDom = function(){
    
    var self = this;
    
    // fill the total number of slides, to be used by the slides counter
    this.$slides.find('> hgroup > h2').attr('data-slides-total', this.$slides.length);
    
    // add navigation arrows
    this.navigationArrows.previous = $('<a href="#" class="arrow-nav arrow-nav-previous" />').click(function(event){
        event.preventDefault();
        self.previous();
    });
    this.$container.append(this.navigationArrows.previous);
    this.navigationArrows.next = $('<a href="#" class="arrow-nav arrow-nav-next" />').click(function(event){
        event.preventDefault();
        self.next();
    });
    this.$container.append(this.navigationArrows.next);
    
}

/**
 * Binds all necessary events to control the slideshow.
 * 
 * @returns {undefined}
 */
AppPrezi.prototype._bind = function(){
    
    var self = this;
    
    $(document).keydown(function(event){
        switch(event.which){
            case 37 /* left arrow */:
                event.preventDefault();
                self.previous();
                break;
            case 13 /* return */:
            case 32 /* space */:
            case 39 /* right arrow */:
                event.preventDefault();
                self.next();
                break;
        }
    });
    
};

/**
 * Resets the slideshow to the first slide.
 * 
 * @returns {undefined}
 */
AppPrezi.prototype.reset = function(){

    // remove all state classes
    this.$slides.removeClass('active previous next');
    // activate the first slide
    this.current = 0;
    this.$slides.first().addClass('active');
    this.$slides.slice(1).addClass('next');
    
    // call the on slide change handler
    this._onSlideChange();
    
};

/**
 * Shows the next slide.
 * 
 * @returns {undefined}
 */
AppPrezi.prototype.next = function(){

    // check if this is the last slide
    if (!this.hasNext())
        return;
    
    // pass the current slide
    this.$slides.eq(this.current).removeClass('active').addClass('previous');

    // activate the next slide
    this.current++;
    this.$slides.eq(this.current).removeClass('next').addClass('active');
    
    // call the on slide change handler
    this._onSlideChange();
    
};

/**
 * Shows the previous slide.
 * 
 * @returns {undefined}
 */
AppPrezi.prototype.previous = function(){
    
    // check if this is the first slide
    if (!this.hasPrevious())
        return;

    // deactivate the current slide
    this.$slides.eq(this.current).removeClass('active').addClass('next');

    // activate the previous slide
    this.current--;
    this.$slides.eq(this.current).removeClass('previous').addClass('active');
    
    // call the on slide change handler
    this._onSlideChange();
    
};

/**
 * Checks if we have reached the end of the slideshow.
 * 
 * @returns {boolean}
 */
AppPrezi.prototype.hasNext = function(){
    
    return (this.current + 1 < this.$slides.length);
    
};

/**
 * Checks if this is the first slide of the slideshow.
 * 
 * @returns {boolean}
 */
AppPrezi.prototype.hasPrevious = function(){
    
    return this.current > 0;
    
};

/**
 * Handler called on each slide change.
 * 
 * @returns {undefined}
 */
AppPrezi.prototype._onSlideChange = function(){

    // manage navigation arrows
    // arrow next
    if (this.hasNext()){
        this.navigationArrows.next.removeClass('disabled');
    }else{
        this.navigationArrows.next.addClass('disabled');
    }
    // arrow previous
    if (this.hasPrevious()){
        this.navigationArrows.previous.removeClass('disabled');
    }else{
        this.navigationArrows.previous.addClass('disabled');
    }
    
};


// initiate
/**
 * @type AppPrezi
 */
var ap;
$(function(){
    ap = new AppPrezi();
});